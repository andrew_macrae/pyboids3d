from visual import *
import numpy as np

# Class definitions

class V3:
    """ Basic class to represent a real-values, 3-vector.
    Vector consists of 3 floats, rather than a numpy array by design choice"""
    def __init__(self,x0,y0,z0):
        self.x = x0
        self.y = y0
        self.z = z0

    def mag(self):
        return np.sqrt(self.x**2 + self.y**2 + self.z**2)

    def cross(self,b):
        '''Cross product'''
        return np.array([self.x,self.y,self.z]).cross(b)

    def scale(self,d):
        return V3(self.x*d, self.y*d, self.z*d)

    def add(self, b):
        return V3(self.x+b.x, self.y+b.y, self.z+b.z)

    def subt(self, b):
        return V3(self.x-b.x, self.y-b.y, self.z-b.z)

    def dot(self, b):
        return self.x*b.x + self.y*b.y + self.z*b.z

    def unit(self):
        if(self.mag()==0):
            return V3(0,0,0)
        else:
            return self.scale(1.0/self.mag())

class Boid3D:
    """ 3D Boid. Holds position and velocity 3-vectors. No drawing is specified"""

    def __init__(self,x0,y0,z0,vx0,vy0,vz0):

        self.berdrad = 6
        self.berdnose = 4
        self.beaklength = 3*self.berdrad

        self.pos = V3(x0,y0,z0)
        self.vel = V3(vx0, vy0, vz0)

        velunit = self.vel.unit().scale(self.beaklength);
        r = np.random.rand()
        g = np.random.rand()
        b = np.random.rand()
        self.body = sphere(pos=vector(x0, y0, z0), radius=self.berdrad,color = (r,g,b))
        self.nose = arrow(pos=vector(x0, y0, z0), axis=vector(velunit.x, velunit.y, velunit.z), shaftwidth=self.berdnose,color = (r,g,b))

    def move(self, dt):
        '''Essentially, uses Euler integration'''

        # First update the vector position ....
        self.pos = self.pos.add(self.vel.scale(dt))

        # Then, update the drawing parameters based on the new vector
        velunit = self.vel.unit().scale(self.beaklength);
        self.body.pos = vector(self.pos.x,self.pos.y,self.pos.z)
        self.nose.pos = vector(self.pos.x,self.pos.y,self.pos.z)
        self.nose.axis = vector(velunit.x,velunit.y,velunit.z)
        return

    def capvel(self, vmax):
        '''Clips the velocity to some max value'''
        mg = self.vel.mag()
        if mg > vmax:
            self.vel = self.vel.scale(vmax/mg)
        return

# ----- Functions ------

# ---- Boid Movement Rules ----

# Rule 1: Follow the center of mass of the flock
def flock2centroid(idx,gain):
    centroid = V3(0,0,0)
    for k in range(nBerds):
        if k!=idx:
            centroid = centroid.add(berds[k].pos)
    centroid = centroid.scale(1.0 / (float(nBerds) - 1.0))
    return centroid.subt(berds[idx].pos).scale(gain)

# Rule 2: Don't bang into each other
def avoidcollsions(idx,gain):
    birdRepellant = V3(0,0,0)
    temp = V3(0, 0, 0)
    maxdist = 50

    for k in range(nBerds):
        if k != idx:
            temp = berds[k].pos.subt(berds[idx].pos)
            if temp.mag() < maxdist:
                den = max(temp.mag(),1.0)
                birdRepellant = birdRepellant.add(temp.scale(-gain/den**2))
    return birdRepellant

# Rule 3: Try to match velocity of nearby boids (velocity follows centroid of velocity)
def flocktogether(idx,gain):
    centroid = V3(0,0,0)
    for k in range(nBerds):
        if k!=idx:
            centroid = centroid.add(berds[k].vel)
    centroid = centroid.scale(1.0 / (float(nBerds) - 1.0))
    return centroid.subt(berds[idx].vel).scale(gain)

# Rule 4: Try to fly at cruise velocity
def velfeedback(idx,setpoint,gain):
    return berds[k].vel.unit().scale(gain*(setpoint - berds[k].vel.mag()))

# Rule 5: stay near the central location
def centralize(idx,x0,y0,z0,gain):
    center = V3(x0,y0,z0)
    return berds[idx].pos.subt(center).scale(-gain)



# ===================================
# ----------- Globals --------------
# ===================================

nBerds = 35
berds  = [] #

region_size = 200
max_vel = 10
DW = region_size/2
wrappedspace = False


# ==================== Main Loop =================================

for k in range(nBerds):
    tx = (np.random.rand() - .5) * region_size
    ty = (np.random.rand() - .5) * region_size
    tz = (np.random.rand() - .5) * region_size
    tvx = (np.random.rand() - .5) * region_size
    tvy = (np.random.rand() - .5) * region_size
    tvz = (np.random.rand() - .5) * region_size

    berds = berds + [Boid3D(tx, ty, tz, tvx, tvy, tvz)]


while True:

    rate(30)
# Think boids ... think
    for k in range(nBerds):
        acc = V3(0,0,0)
    # Apply rules
        acc = acc.add(flock2centroid(k, 2.5e-2))
        acc = acc.add(avoidcollsions(k, 2.5e1))
        acc = acc.add(flocktogether(k, 2.5e-2))
        acc = acc.add(velfeedback(k,30,5e-2))
        acc = acc.add(centralize(k, 0, 0 , 0, 5e-3))

        berds[k].vel = berds[k].vel.add(acc)
        berds[k].capvel(42.2)

# Move 'dem boids
    for k in range(nBerds):
        berds[k].move(.05)
    # Wrap space (toroidal mapping)
        if wrappedspace:
            if berds[k].pos.x > DW:
                berds[k].pos.x -= region_size
            if berds[k].pos.x < -DW:
                berds[k].pos.x += region_size
            if berds[k].pos.y > DW:
                berds[k].pos.y -= region_size
            if berds[k].pos.y < -DW:
                berds[k].pos.y += region_size
            if berds[k].pos.z > DW:
                berds[k].pos.z -= region_size
            if berds[k].pos.z < -DW:
                berds[k].pos.z += region_size