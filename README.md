# README #

### What is this repository for? ###

* 3D python version of the classic Boids algorithm

* Version 1.0

### How do I get set up? ###

For this baby to run you need vpython (visual python) installed on your system.
vpython can be installed [here](http://vpython.org).

Tested and developed using Python 2.7